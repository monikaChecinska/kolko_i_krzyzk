var model = (function() {
  var turn = 1,
    isFirstPlayer = function() {
      var result = turn == 1;
      if (result) {
        turn = 2;
      } else {
        turn = 1;
      }
      return result;
    };
  return {
    isFirstPlayer: isFirstPlayer
  }
})();
