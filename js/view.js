var view = (function() {
  var firstPlayerHit = function(element) {
      element.classList.add('first-player');
      element.innerHTML = 'o';
    },
    secondPlayerHit = function(element) {
      element.classList.add('second-player');
      element.innerHTML = 'x';
    },
    setFirstPlayer = function() {
        var element = document.getElementById("which-player");
        element.value = "First player";
    },
    setSecondPlayer = function() {
        var element = document.getElementById("which-player");
        element.value = "Second player";
    },
    addElement = function (onClickFunction) {
      var div = document.createElement("DIV"),
          lastRow = document.getElementById('generated-elements');
      div.onclick = onClickFunction;
      div.className = 'generated-item';
      lastRow.appendChild(div);
    };
  return {
    firstPlayerHit: firstPlayerHit,
    secondPlayerHit: secondPlayerHit,
    setFirstPlayer: setFirstPlayer,
    setSecondPlayer: setSecondPlayer,
    addElement: addElement
  }
})();
