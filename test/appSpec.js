describe('tests for app.js', function() {
  it('should return correct player turn', function() {
    // given
    var firstResult,
    secondResult;

    // when
    firstResult = model.isFirstPlayer();
    secondResult = model.isFirstPlayer();
    thirdResult = model.isFirstPlayer();
    // then
    expect(firstResult).toBe(true);
    expect(secondResult).toBe(false);
    expect(thirdResult).toBe(true);

  });
});
